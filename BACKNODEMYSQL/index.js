require('dotenv').config();

const express = require('express');
const app = express();
const db = require('./database');

const cors = require('cors');
var corsOptions = {
    origin: '*', // Reemplazar con dominio
    optionsSuccessStatus: 200 //
}

app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get('user/', db.getUser);
app.post('user/', db.setUser);
app.post('user/update', db.updateUser);
app.get('user/:id', db.deleteUser);

app.listen(process.env.PORT, function () {
    console.log('app listening at port %s', process.env.PORT);
});

module.exports = app
