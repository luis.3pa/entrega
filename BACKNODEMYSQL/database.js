const mysql = require('mysql');

const pool = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'my_secret_password',
    database: 'app_db',
    multipleStatements: true
});
pool.connect(function (err) {
    if (err) {
        console.log('Connection Failed!' + JSON.stringify(err, undefined, 2));
        throw err
    }
    console.log("Database Connected!");
});


const getUser = (request, response) => {
    pool.query('SELECT * FROM user', (error, results) => {
        if (error) {
            throw error;
        }
        response.status(200).json(results.rows)
    })
}



const setUser = (request, response) => {
    const { nombres, apellidos, email, cedula, telefono } = request.body

    pool.query('SELECT * FROM user where id=$1'[cedula], (error, results) => {
        if (error) {
            throw error;
        }
        results.forEach(element => {
            if (id != element.id) {
                return res.status(400).json({ error: 'Cedula ya registrada' })
            }
        });
    })

    pool.query('SELECT * FROM user where id=$1'[email], (error, results) => {
        if (error) {
            throw error;
        }
        results.forEach(element => {
            if (id != element.id) {
                return res.status(400).json({ error: 'Email ya registrado' })
            }
        });
    })

    pool.query('INSERT INTO user (nombres, apellidos,email,cedula,telefono) VALUES ($1, $2)', [nombres, apellidos, email, cedula, telefono], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`Usuario agregada con ID: ${result.insertId}`)
    })
}


const updateUser = (request, response) => {
    const { id, nombres, apellidos, email, cedula, telefono } = request.body

    pool.query('SELECT * FROM user where id=$1'[cedula], (error, results) => {
        if (error) {
            throw error;
        }
        results.forEach(element => {
            if (id != element.id) {
                return res.status(400).json({ error: 'Cedula ya registrada' })
            }
        });
    })

    pool.query('SELECT * FROM user where id=$1'[email], (error, results) => {
        if (error) {
            throw error;
        }
        results.forEach(element => {
            if (id != element.id) {
                return res.status(400).json({ error: 'Email ya registrado' })
            }
        });
    })

    pool.query('UPDATE user SET nombres=$2,apellidos=$3,email=$4,cedula=$5,telefono=$6 WHERE id=$1', [id, nombres, apellidos, email, cedula, telefono], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`Usuario actualizado con ID: ${result.insertId}`)
    })
}


const deleteUser = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('DELETE FROM user WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

module.exports = {
    getUser,
    setUser,
    updateUser,
    deleteUser
}