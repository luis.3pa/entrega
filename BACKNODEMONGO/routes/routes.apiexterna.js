
const { Router } = require('express');
const router = Router();
const fetch = require('node-fetch');

router.get('/', async (req, res) => {
   (fetch('https://jsonplaceholder.typicode.com/photos')
        .then(response => response.json())
        .then(json =>  res.json(json)));


});

module.exports = router;