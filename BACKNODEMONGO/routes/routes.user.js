const { Router } = require('express');
const router = Router();

const User = require('../models/user.model');
const bcrypt = require('bcryptjs');


router.get('/', async (req, res) => {
    const users = await User.find();
    res.json(users);
});

router.post('/', async (req, res) => {

    const isEmailExist = await User.findOne({ email: req.body.email });
    if (isEmailExist) {
        return res.status(400).json({ error: 'Email ya registrado' })
    }

    const isCedulaExist = await User.findOne({ cedula: req.body.cedula });

    if (isCedulaExist) {
        return res.status(400).json({ error: 'Cedula ya registrada' })
    }

    const newUser = new User({
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        cedula: req.body.cedula,
        email: req.body.email,
        telefono: req.body.telefono
    });

    try {
        const savedUser = await newUser.save();
        res.json({
            error: null,
            data: savedUser
        })
    } catch (error) {
        res.status(400).json({ error })
    }
});

router.post('/update', async (req, res) => {

    const isEmailExist = await User.findOne({ email: req.body.email });

    if (isEmailExist) {
        if (req.body.id != isEmailExist._id) {
            return res.status(400).json({ error: 'Email ya registrado' })
        }
    }

    const isCedulaExist = await User.findOne({ cedula: req.body.cedula });

    if (isCedulaExist) {
        if (req.body.id != isCedulaExist._id) {
            return res.status(400).json({ error: 'Cedula ya registrada' })
        }
    }

    try {
        const savedUser = await User.findOne({ _id: req.body.id }, (err, doc) => {
            //this will give you the document what you want to update.. then 
            doc.nombres = req.body.nombres;
            doc.apellidos = req.body.apellidos;
            doc.cedula = req.body.cedula;
            doc.email = req.body.email;
            doc.telefono = req.body.telefono;

            doc.save();
        });
        res.json({
            error: null,
            data: savedUser
        })
    } catch (error) {
        res.status(400).json({ error })
    }
});

router.get('/:id', async (req, res) => {
    await User.findByIdAndDelete(req.params.id);
    res.send({ message: 'Usuario eliminado' });
});

router.get('detail/:id', async (req, res) => {
    const users = await User.findById(req.params.id);
    res.json(users);
});

module.exports = router;