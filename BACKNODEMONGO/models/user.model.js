const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  nombres: String,
  apellidos: String,
  cedula: String,
  email: String,
  telefono: Number
});

module.exports = model('User', userSchema);

