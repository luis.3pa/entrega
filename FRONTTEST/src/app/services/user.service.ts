import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';


const cabecera = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private httpClient: HttpClient) { }

  public get(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.api}/user`, cabecera);
  }

  public set(user:User): Observable<User[]> {
    return this.httpClient.post<User[]>(`${environment.api}/user`,user, cabecera);
  }

  public update(user:User): Observable<User[]> {
    return this.httpClient.post<User[]>(`${environment.api}/user/update`,user, cabecera);
  }

  public getId(id:string): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.api}/detail/${id}`, cabecera);
  }

  public deleteId(id:string): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.api}/user/${id}`, cabecera);
  }

}