import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

declare var jQuery: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('createModal', { static: false }) createModal: ElementRef;
  @ViewChild('delete', { static: false }) deleteUser: ElementRef;
  users: User[];

  form: FormGroup;
  update = false;
  labelTitleModal = "";
  submitted = false;
  userDelete: string = "";

  settings = {};

  constructor(private userService: UserService,
    private formBuilder: FormBuilder,
    private toastrService: NbToastrService,
    private router: Router) {
    this.buildForm();
    this.labelTitleModal = "Nuevo Usuario";
  }

  ngOnInit(): void {

    this.service();
    this.settings = {

      actions: {

        add: false,
        edit: false,
        delete: false,
        custom: [
          {
            name: 'edit', title: `<small class="fa fa-pencil"></small>`
          },
          {
            name: 'delete', title: `<small class="fa fa-trash-o"></small>`
          },
        ],
        position: 'right'
      },
      columns: {
        _id: {
          title: 'ID'
        },
        nombres: {
          title: 'Nombre'
        },
        apellidos: {
          title: 'Apellido',
        },
        cedula: {
          title: 'Cedula',
        },
        email: {
          title: 'correo',
        },
        telefono: {
          title: 'Telefono',
        }

      }
    };
  }

  service() {
    this.userService.get().subscribe(data => {
      this.users = data;
    });

  }

  buildForm() {
    this.form = this.formBuilder.group({
      nombres: ['', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]],
      apellidos: ['', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]],
      cedula: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      email: ['', [Validators.required, Validators.email]],
      telefono: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
    });

  }

  change(element: User) {
    this.update = true;
    this.labelTitleModal = "Editar Usuario";
    this.form = this.formBuilder.group({
      nombres: [element.nombres, [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]],
      apellidos: [element.apellidos, [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]],
      cedula: [element.cedula, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      email: [element.email, [Validators.required, Validators.email]],
      telefono: [element.telefono, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      id: [element._id, Validators.required],
    });
  }

  loalDelete(element: User) {
    this.userDelete = element._id;
  }

  save() {
    this.submitted = true;
    if (this.form.valid) {
      let user: User = this.form.value;
      if (this.update) {
        this.userService.update(user).subscribe(data => {
          this.service();
          jQuery(this.createModal.nativeElement).modal('hide');
          this.toastrService.show("", `Actualizado`, { limit: 3, position: NbGlobalLogicalPosition.TOP_END, status: 'success' });
        },
          (err: any) => {
            jQuery(this.createModal.nativeElement).modal('hide');
            this.toastrService.show(err.error.error, `Upps`, { limit: 3, position: NbGlobalLogicalPosition.TOP_END, status: 'warning' });
            console.log(err);
          });
      } else {
        this.userService.set(user).subscribe(data => {
          this.service();
          jQuery(this.createModal.nativeElement).modal('hide');
          this.toastrService.show("", `Guardado`, { limit: 3, position: NbGlobalLogicalPosition.TOP_END, status: 'success' });
        },
          (err: any) => {
            jQuery(this.createModal.nativeElement).modal('hide');
            this.toastrService.show(err.error.error, `Upps`, { limit: 3, position: NbGlobalLogicalPosition.TOP_END, status: 'warning' });
            console.log(err);
          });
      }

    }
  }

  onCustomAction(event) {

    let data: User = <User>event.data;
    switch (event.action) {
      case 'edit':
        this.change(data);
        jQuery(this.createModal.nativeElement).modal('show');
        break;
      case 'delete':
        this.loalDelete(data);
        jQuery(this.deleteUser.nativeElement).modal('show');
        break;

    }

  }

  delete2() {
    this.userService.deleteId(this.userDelete).subscribe(data => {
      this.service();
      jQuery(this.deleteUser.nativeElement).modal('hide');
      this.toastrService.show("", `Eliminado`, { limit: 3, position: NbGlobalLogicalPosition.TOP_END, status: 'danger' });
    });
  }

}
