export class User {
    _id: string;
    nombres: string;
    apellidos: string;
    cedula: string;
    email: string;
    telefono: string;
}
